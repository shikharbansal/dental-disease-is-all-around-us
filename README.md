**Dental Disease Is All Around Us**

Tooth decay is most prevalent health problem.

11 million newly decayed teeth appear every year. 

With 90% of dental disease easily preventable through the adoption of simple modifiable habits, we are collectively asleep at the wheel as we hurtle to a national health disaster.
Good [smile makeover in Mexico](http://mexicodental.network/) is central to the overall well being of our people, economy and our nation.

The problem is that as a society we have grown up in a culture of misunderstanding exactly how important it is.   
Newsflash!  Oral Health Affects Your Life Expectancy

The problem is that  we see dental health as not being a central part of our overall health and wellbeing.

If the thought of dying from a tooth issue sounds ridiculous, here are the facts.  People with advanced gum disease are twice as likely to die before age 64 than their mouth-healthy counterparts. 
You may not be laughing so hard now.

The mouth is an integral and vital part of your body linked to life-threatening diseases.
A Culture Of Dental Neglect
55% of Australian’s brush their teeth twice a day.

57% of people feel they will inevitably get tooth decay.

How has this culture of treating dental health as a hindrance rather than a necessity develop? 
At a government level we see dental health treated as a secondary importance. 

A review of the reforms to public dental policy and funding over the past 20 years show little impact on the burden of oral disease since water fluoridation.    
The problem here is that politicians are drawn to issues that the nation talks about.  

We’re clearly not talking enough about dental disease.
It Also Makes Us Poorer – Unnecessarily


We are paying dearly for this indifference to oral disease with direct and indirect costs of dental disease exceeding $2 billion annually.
As in all health-care, prevention is better than the cure.  Every year Australians spend $9 billion on dental treatment.

For every dollar invested into prevention of dental disease, seven dollars are saved in treatment costs.  
The math is simple when we consider the ways that savings could be redistributed to improve the health and productivity for Australian lives. 
Old Habits Die Hard


Uprooting an entire culture is not something that can be achieved overnight.  If we are to change the future of our dental health, a revolution needs to happen.     
Specifically this revolution involves shifting away from the focus on corrective and reparative treatments to one of prevention, diagnosis and early intervention. 
Through acknowledgement of the underlying problems, we can effectively identify the best steps forward. 


Knowledge Is The Missing Link
Scientific research has long acknowledged the inextricable link between education and prevention – without knowledge people are unable to adopt crucial lifestyle habits in disease modification.
Oral health needs to be brought to the forefront of our consciousness through positive and effective communication. 

Visit: [Smile makeover in Mexico](http://egdentalmex.com/)

